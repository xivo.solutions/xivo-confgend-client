xivo-confgend-client
====================

[![Build Status](https://travis-ci.org/xivo-pbx/xivo-confgend-client.png?branch=master)](https://travis-ci.org/xivo-pbx/xivo-confgend-client)

xivo-confgend-client is a client for xivo-confgend service for generating IPBX configuration files
